#pragma once

#include <thread>

#include <lumin/LuminRuntime.h>
#include <lumin/ui/UiKit.h>

#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <gst/gl/gl.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

class VideoScene : public lumin::PrismController {
public:
  VideoScene(lumin::BaseApp &app, const std::string &uri,
      uint32_t width, uint32_t height, bool stereoscopic, float widthFactor = 1.0f);
  void pause();
  void resume();

private:
  void onAttachPrism(lumin::Prism* prism) override;
  void onDetachPrism(lumin::Prism* prism) override;
  void onUpdate(float delta) override;

  void threadLoop();

  static GstBusSyncReply busSyncHandlerCb(GstBus *bus, GstMessage *msg, gpointer user_data);
  void createPipeline();
  void createGLTextureAndProgram();
  void renderGLTexture();
  bool renderVideoFrame();
  void resetAudioPosition();
  void resize();
  void seekTo(float position);

  std::string mUri;
  lumin::BaseApp &mApp;
  lumin::PlanarResource *mPlanar;
  lumin::QuadNode *mQuad;
  lumin::ui::UiSlider *mSlider;
  lumin::AudioNode *mAudioNode = nullptr;

  EGLDisplay mEGLDisplay = EGL_NO_DISPLAY;
  EGLContext mEGLContext = EGL_NO_CONTEXT;
  EGLSurface mEGLSurface = EGL_NO_SURFACE;
  GLuint mFragmentShaderId = 0;
  GLuint mVertexShaderId = 0;
  GLuint mProgramId = 0;
  GLuint mTextureId = 0;
  GLenum mTextureTarget = 0;

  GstGLDisplay *mDisplay = NULL;
  GstGLContext *mContext = NULL;
  GstGLContext *mOtherContext = NULL;
  GstElement *mPlayBin = NULL;
  GstElement *mAppSink = NULL;

  uint32_t mWidth;
  uint32_t mHeight;
  bool mStereoscopic;
  float mWidthFactor;
  bool mPlaying = false;

  std::thread mThread;
  std::atomic<bool> mRunning;
};
