# Requirements
- Magic Leap SDK 0.24.x for MacOSX
  * Download from https://creator.magicleap.com/downloads/lumin-sdk/overview
  * Install both `Lumin SDK` and `Lumin Runtime SDK` packages
- An application certificate
  * Create one on https://creator.magicleap.com in `publish` section

# System Setup on MacOSX
- Install python3 and HomeBrew
- pip3 install git+https://github.com/mesonbuild/meson.git
  * Requires Meson >=0.52.0, currently only in git master.
- brew install coreutils glib bison
- export PATH=/usr/local/opt/gettext/bin:/usr/local/opt/bison/bin:$PATH

# Build Instructions

```
export MAGICLEAP_SDK=/path/to/mlsdk
export MLCERT=/path/to/application.cert
./build.sh
```

For internal testing, the `ml_internal` certificate can be used, e.g.:
```
export MAGICLEAP_SDK="$HOME/MagicLeap-SDK/mlsdk"
export MLCERT="$MAGICLEAP_SDK/internal/ml_internal"
./build.sh
```

Install the demo:
```
mldb install -u .out/gstreamer_demo/gstreamer_demo.mpk
```

Push the test media to the device:

```
wget http://automationabox42app.com/bunny/big_buck_bunny_720p_h264.mp4
mldb push -p com.collabora.gstreamer_demo big_buck_bunny_720p_h264.mp4 /documents/C1/
```

Launch the demo:
```
mldb launch com.collabora.gstreamer_demo

```
