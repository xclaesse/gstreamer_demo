#include <list>
#include <memory>

#include <ml_logging.h>
#include <lumin/LuminRuntime.h>
#include <lumin/ui/UiKit.h>

#include "VideoScene.h"
#include <gst/gstinitstaticplugins.h>

using namespace lumin;
using namespace lumin::ui;

class GStreamerDemo : public LandscapeApp {
public:
  GStreamerDemo();

private:
  int init() override;
  int deInit() override;
  void onAppStart(const InitArg& initArg) override;
  void onAppPause() override;
  void onAppResume() override;

  void addVideo(const std::shared_ptr<VideoScene> &videoScene);

  std::list<std::shared_ptr<VideoScene>> mVideos;
};

static void gstreamer_log_cb(GstDebugCategory *category, GstDebugLevel level,
    const gchar *file, const gchar *function, gint line, GObject *object,
    GstDebugMessage *message, gpointer user_data)
{
  ML_LOG(Info, "%s:%d %s() %s", file, line, function,
      gst_debug_message_get(message));
}

static void glib_log_cb(const gchar *log_domain, GLogLevelFlags log_level,
    const gchar *message, gpointer user_data)
{
  ML_LOG(Info, "%s: %s", log_domain, message);
}

GStreamerDemo::GStreamerDemo()
{
}

int GStreamerDemo::init()
{
  auto tmpdir = getTempPath();
  auto bindir = getPackagePath() + "bin";
  auto registry = getWritablePath() + "gstreamer-registry.bin";
  setenv("GIO_MODULE_DIR", bindir.c_str(), 1);
  setenv("GST_PLUGIN_SYSTEM_PATH", bindir.c_str(), 1);
  setenv("GST_REGISTRY", registry.c_str(), 1);
  setenv("XDG_CACHE_HOME", tmpdir.c_str(), 1);
  setenv("GST_DEBUG", "3", 1);
  setenv("G_MESSAGES_DEBUG", "all", 1);

  gst_debug_add_log_function(gstreamer_log_cb, NULL, NULL);
  g_log_set_default_handler(glib_log_cb, NULL);
  gst_init(NULL, NULL);
  gst_init_static_plugins();

  return 0;
}

int GStreamerDemo::deInit()
{
  return 0;
}

void GStreamerDemo::onAppStart(const InitArg& initArg)
{
  if (mVideos.empty()) {
    addVideo(std::make_shared<VideoScene>(*this,
        "file:///documents/C1/big_buck_bunny_720p_h264.mp4", 1280, 720, false));
  }
}

void GStreamerDemo::onAppPause()
{
  ML_LOG(Info, "pause");
  for (auto &videoScene : mVideos) {
    videoScene->pause();
  }
}

void GStreamerDemo::onAppResume()
{
  ML_LOG(Info, "resume");
  for (auto &videoScene : mVideos) {
    videoScene->resume();
  }
}

void GStreamerDemo::addVideo(const std::shared_ptr<VideoScene> &videoScene)
{
  auto volExtents = glm::vec3(2.0f, 2.0f, 0.5f) * 0.5f;
  auto prism = requestNewPrism(volExtents);
  disableContentPersistence(prism);
  setCollisionsEnabled(prism, true);
  prism->setPrismController(videoScene);
  mVideos.push_back(videoScene);
}

int main(int argc, char **argv) {
  GStreamerDemo app;
  int ret = app.run();
  ML_LOG(Info, "quit");
  return ret;
}
